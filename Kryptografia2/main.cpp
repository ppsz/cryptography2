#include "Header.h"

vector g_message(8);

int main()
{
	vector message{ 1,1,1,1,0,0,0,0 };
	vector initial_key = { 1,1,0,0,0,0,0,0,1,1 };
	vector PW_scheme = { 1,5,2,0,3,7,4,6 };
	vector P10_scheme = { 2,4,1,6,3,9,0,8,7,5 };
	vector P10w8_scheme = { 5,2,6,3,7,4,9,8 };
	vector P4w8_scheme = { 3,0,1,2,1,2,3,0 };
	vector P4_scheme = { 1,3,2,0 };
	vector PO_scheme = { 3,0,2,4,6,1,7,5 };
	int menu_select;

	while (message.size() % 8 != 0)
		message.push_back(0);

	//Utworzenie kluczy
	PermutationP10(initial_key, P10_scheme);
	RotateLeft();
	PermutationP10w8(P10w8_scheme);

	for (;;)
	{
		std::cout
			<< "|---------------------------------|\n"
			<< "|           Menu Glowne           |\n"
			<< "|---------------------------------|\n"
			<< "| 1: Zaszyfruj                    |\n"
			<< "|                                 |\n"
			<< "| 2: Odszyfruj                    |\n"
			<< "|                                 |\n"
			<< "| 0: Wyjdz z programu             |\n"
			<< "|---------------------------------|\n";

		menu_select = GetValue("   Wybierz pozycje menu: ", 0, 3, false);

		switch (menu_select)
		{
		case 1:
			//Szyfrowanie
			for (int i = 0; i < message.size(); i += 8)
			{
				for (int j = 0; j < 8; ++j)
					g_message[j] = message[i + j];
				PermutationPW(PW_scheme);
				//Pierwsza runda
				SplitMessage();
				PermutationP4w8(P4w8_scheme);
				FirstRoundXOR();
				SBoxFunction();
				PermutationP4(P4_scheme);
				SwitchLeftRight();

				//Druga runda
				SplitMessage();
				PermutationP4w8(P4w8_scheme);
				SecondRoundXOR();
				SBoxFunction();
				PermutationP4(P4_scheme);
				PermutationPO(PO_scheme);

				for (int j = 0; j < 8; ++j)
					message[i+j] = g_message[j];

				for (int j = 0; j < 8; ++j)
					std::cout << message[i+j];
			}
			std::cout << "\n\n";
			break;
		case 2:
			//Odszyfrowanie
			for (int i = 0; i < message.size(); i += 8)
			{
				for (int j = 0; j < 8; ++j)
					g_message[j] = message[i + j];

				PermutationPW(PW_scheme);
				//Druga runda
				SplitMessage();
				PermutationP4w8(P4w8_scheme);
				SecondRoundXOR();
				SBoxFunction();
				PermutationP4(P4_scheme);
				SwitchLeftRight();

				//Pierwsza runda
				SplitMessage();
				PermutationP4w8(P4w8_scheme);
				FirstRoundXOR();
				SBoxFunction();
				PermutationP4(P4_scheme);
				PermutationPO(PO_scheme);

				for (int j = 0; j < 8; ++j)
					message[i + j] = g_message[j];

				for (int j = 0; j < 8; ++j)
					std::cout << message[i + j];
			}
			std::cout << "\n\n";
			break;
		case 0:
			return 0;
			break;
		default:
			return -1;
		}
	}
	system("pause");
	return 0;
}