#include "getvalue.h"

int GetValue(std::string message_to_print, int min, int max, bool clear_after_mistake)
{
	for (;;)
	{
		int value;
		std::cout << message_to_print;
		std::cin >> value;

		if (std::cin.good() && value >= min && value <= max)
		{
			system("cls");
			return value;
		}
		else
		{
			if(clear_after_mistake) system("cls");
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "Podana liczba musi byc liczba z zakresu <" << min << ":" << max << ">" << std::endl;
			continue;
		}
	}
}