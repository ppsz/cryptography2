#pragma once
#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include "getvalue.h"

typedef std::vector<short> vector;

extern vector g_message;

//Permutacja wstepna
void PermutationPW(vector scheme);

//Permutacja P10
void PermutationP10(vector key, vector scheme);

//Obrot w lewo o 1, potem o 2 odpowiednich ciagow
void RotateLeft();

//Permutacja P10w8
void PermutationP10w8(vector scheme);

//Podzielenie wiadomosci na dwa bloki czterobitowe
void SplitMessage();

//Permutacja P4w8
void PermutationP4w8(vector scheme);

//Dodanie binarne klucza pierwszej rundy do ciagu po P4w8
void FirstRoundXOR();
//Dodanie binarne klucza drugiej rundy do ciagu po P4w8
void SecondRoundXOR();

//Wykonanie funkcji SBox1 i SBox2
void SBoxFunction();

//Permutacja P4, dodanie tego ciagu binarnie do 4 pierwszych bitow wiadmosci, wynik
void PermutationP4(vector scheme);

//Zamiana 4 pierwszych bitow z 4 ostatnimi bitami wiadomosci
void SwitchLeftRight();

//Permutacja odwrotna
void PermutationPO(vector scheme);