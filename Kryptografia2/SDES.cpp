#include "Header.h"

vector g_key_after_P10(10);
vector g_first_round_key_10(10);
vector g_second_round_key_10(10);
vector g_first_round_key(8);
vector g_second_round_key(8);
vector g_message_left(4);
vector g_message_right(4);
vector g_message_after_P4w8(8);
vector g_message_after_P4w8_left(4);
vector g_message_after_P4w8_right(4);
vector g_message_after_P4(4);

std::map<vector, vector> SBox1 = {
	{ { 0,0,0,0 },{ 0,1 } },{ { 0,0,1,0 },{ 0,0 } },{ { 0,1,0,0 },{ 1,1 } },{ { 0,1,1,0 },{ 1,0 } },
	{ { 0,0,0,1 },{ 1,1 } },{ { 0,0,1,1 },{ 1,0 } },{ { 0,1,0,1 },{ 0,1 } },{ { 0,1,1,1 },{ 0,0 } },
	{ { 1,0,0,0 },{ 0,0 } },{ { 1,0,1,0 },{ 0,1 } },{ { 1,1,0,0 },{ 0,1 } },{ { 1,1,1,0 },{ 1,1 } },
	{ { 1,0,0,1 },{ 1,1 } },{ { 1,0,1,1 },{ 0,1 } },{ { 1,1,0,1 },{ 1,1 } },{ { 1,1,1,1 },{ 1,0 } } };
std::map<vector, vector> SBox2 = {
	{ { 0,0,0,0 },{ 0,0 } },{ { 0,0,0,1 },{ 1,0 } },{ { 0,0,1,0 },{ 0,1 } },{ { 0,0,1,1 },{ 0,0 } },
	{ { 0,1,0,0 },{ 1,0 } },{ { 0,1,0,1 },{ 0,1 } },{ { 0,1,1,0 },{ 1,1 } },{ { 0,1,1,1 },{ 1,1 } },
	{ { 1,0,0,0 },{ 1,1 } },{ { 1,0,0,1 },{ 1,0 } },{ { 1,0,1,0 },{ 0,0 } },{ { 1,0,1,1 },{ 0,1 } },
	{ { 1,1,0,0 },{ 0,1 } },{ { 1,1,0,1 },{ 0,0 } },{ { 1,1,1,0 },{ 0,0 } },{ { 1,1,1,1 },{ 1,1 } } };

void PermutationPW(vector scheme)
{
	vector message(8);
	for (int i = 0; i < 8; ++i)
		message[i] = g_message[scheme[i]];
	g_message = message;
}

void PermutationP10(vector key, vector scheme)
{
	for (int i = 0; i < 10; ++i)
		g_key_after_P10[i] = key[scheme[i]];
}

void RotateLeft()
{
	vector key_after_P10_left(5);
	vector key_after_P10_right(5);

	for (int i = 0; i < 5; ++i)
	{
		key_after_P10_left[i] = g_key_after_P10[i];
		key_after_P10_right[i] = g_key_after_P10[i + 5];
	}

	std::rotate(key_after_P10_left.begin(), key_after_P10_left.begin() + 1, key_after_P10_left.end());
	std::rotate(key_after_P10_right.begin(), key_after_P10_right.begin() + 1, key_after_P10_right.end());

	for (int i = 0; i < 5; ++i)
	{
		g_first_round_key_10[i] = key_after_P10_left[i];
		g_first_round_key_10[i + 5] = key_after_P10_right[i];
	}

	std::rotate(key_after_P10_left.begin(), key_after_P10_left.begin() + 2, key_after_P10_left.end());
	std::rotate(key_after_P10_right.begin(), key_after_P10_right.begin() + 2, key_after_P10_right.end());

	for (int i = 0; i < 5; ++i)
	{
		g_second_round_key_10[i] = key_after_P10_left[i];
		g_second_round_key_10[i + 5] = key_after_P10_right[i];
	}
}

void PermutationP10w8(vector scheme)
{
	for (int i = 0; i < 8; ++i)
	{
		g_first_round_key[i] = g_first_round_key_10[scheme[i]];
		g_second_round_key[i] = g_second_round_key_10[scheme[i]];
	}
}

void SplitMessage()
{
	for (int i = 0; i < 4; ++i)
	{
		g_message_left[i] = g_message[i];
		g_message_right[i] = g_message[i + 4];
	}
}

void PermutationP4w8(vector scheme)
{
	for (int i = 0; i < 8; i++)
		g_message_after_P4w8[i] = g_message_right[scheme[i]];
}

void FirstRoundXOR()
{
	for (int i = 0; i < 8; ++i)
		g_message_after_P4w8[i] ^= g_first_round_key[i];
}
void SecondRoundXOR()
{
	for (int i = 0; i < 8; ++i)
		g_message_after_P4w8[i] ^= g_second_round_key[i];
}

void SBoxFunction()
{
	vector message_left_2;
	vector message_right_2;
	for (int i = 0; i < 4; ++i)
	{
		g_message_after_P4w8_left[i] = g_message_after_P4w8[i];
		g_message_after_P4w8_right[i] = g_message_after_P4w8[i + 4];
	}

	for (const auto &p : SBox1)
		if (p.first == g_message_after_P4w8_left)
			message_left_2 = p.second;
	for (const auto &p : SBox2)
		if (p.first == g_message_after_P4w8_right)
			message_right_2 = p.second;

	for (int i = 0; i < 2; ++i)
	{
		g_message_after_P4[i] = message_left_2[i];
		g_message_after_P4[i + 2] = message_right_2[i];
	}
}

void PermutationP4(vector scheme)
{
	vector message_4(4);
	for (int i = 0; i < 4; ++i)
		message_4[i] = g_message_after_P4[scheme[i]];
	g_message_after_P4 = message_4;
	for (int i = 0; i < 4; ++i)
		g_message_after_P4[i] ^= g_message_left[i];
	for (int i = 0; i < 4; ++i)
	{
		g_message[i] = g_message_after_P4[i];
		g_message[i + 4] = g_message_right[i];
	}
}

void SwitchLeftRight()
{
	vector message_left(4);
	vector message_right(4);
	for (int i = 0; i < 4; ++i)
	{
		message_left[i] = g_message[i];
		message_right[i] = g_message[i + 4];
	}
	for (int i = 0; i < 4; ++i)
	{
		g_message[i] = message_right[i];
		g_message[i + 4] = message_left[i];
	}
}

void PermutationPO(vector scheme)
{
	vector message(8);
	for (int i = 0; i < 8; ++i)
		message[i] = g_message[scheme[i]];
	g_message = message;
}